# MIT License

# Copyright (c) 2020 Haofan Zheng

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import json
import logging
import socketserver

from . import Network

NOTE_HANDLER_SINGLETON = None

def SetNoteHandler(noteHdl):
	global NOTE_HANDLER_SINGLETON
	NOTE_HANDLER_SINGLETON = noteHdl

class UdpHandler(socketserver.BaseRequestHandler):

	def setup(self):
		global NOTE_HANDLER_SINGLETON

		self.logger = logging.getLogger('LocalDnsMonitor.UdpHandler')

		if NOTE_HANDLER_SINGLETON is None:
			errMsg = 'Notification handler has not been assigned to UDP handler.'
			self.logger.error(errMsg)
			raise Exception(errMsg)

	def handle(self):
		udpData = self.request[0]
		#socket = self.request[1]

		jsonStr = Network.JsonOverUdp.StrUdpPackFromJsonStr(udpData).payload

		noteInJson = json.loads(jsonStr)

		NOTE_HANDLER_SINGLETON.Handle(noteInJson)
