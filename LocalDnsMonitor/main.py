# MIT License

# Copyright (c) 2020 Haofan Zheng

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import socket
import struct
import logging
import ipaddress
import socketserver

import pandas

from . import Config
from . import DbConnector
from . import Cache
from . import Monitor
from . import UdpHandler

def HumanReadableTagCacheRow(row):

	ipFrom = ipaddress.ip_address(row[Cache.Names.IP2LOC_COL_IP_FROM])
	ipTo = ipaddress.ip_address(row[Cache.Names.IP2LOC_COL_IP_TO])
	return pandas.Series(
		[row[Cache.Names.IP2LOC_COL_UID],
		str(ipFrom),
		str(ipTo),
		row[Cache.Names.IP2LOC_COL_TAG]],

		index=[Cache.Names.IP2LOC_COL_UID,
		Cache.Names.IP2LOC_COL_IP_FROM,
		Cache.Names.IP2LOC_COL_IP_TO,
		Cache.Names.IP2LOC_COL_TAG])

def HumanReadableDomainCacheRow(row):

	ipFrom = ipaddress.ip_address(row[Cache.Names.IP2LOC_COL_IP_FROM])
	ipTo = ipaddress.ip_address(row[Cache.Names.IP2LOC_COL_IP_TO])
	return pandas.Series(
		[row[Cache.Names.IP2LOC_COL_UID],
		str(ipFrom),
		str(ipTo),
		row[Cache.Names.IP2LOC_COL_DOMAIN]],

		index=[Cache.Names.IP2LOC_COL_UID,
		Cache.Names.IP2LOC_COL_IP_FROM,
		Cache.Names.IP2LOC_COL_IP_TO,
		Cache.Names.IP2LOC_COL_DOMAIN])

def main(configPath):

	logger = logging.getLogger('LocalDnsMonitor.Main')

	cfg = Config.ReadConfig(configPath)

	##############################
	logger.info('Initializing remote database connector...')
	dbConnector_v4 = None
	if cfg['DB']['db_type'] == 'postgres':
		dbConnector_v4 = DbConnector.Postgres.Postgres(
			addr   = cfg['DB']['addr'],
			port   = cfg['DB']['port'],
			svr_ca = cfg['DB']['svr_ca'],

			user    = cfg['DB']['user'],
			sslkey  = cfg['DB']['sslkey'],
			sslcert = cfg['DB']['sslcert'],

			db          = cfg['DB']['db'],
			mainTable   = cfg['DB']['main_table'],
			tagTable    = cfg['DB']['tagmap_table'],
			domainTable = cfg['DB']['domain_table']
		)
	else:
		logger.error('DbConnector type {type} is not supported in current implementation.'.format(type = cfg['DB']['db_type']))
		exit(-1)

	##############################
	logger.info('Cleaning existing local database cache...')
	if os.path.isfile(cfg['local_cache_path']):
		os.remove(cfg['local_cache_path'])

	##############################
	logger.info('Initializing local database cache...')
	dataFrame = dbConnector_v4.GetTagMapCacheTable()
	tagCache_v4 = Cache.TagMap.TagMap(dataFrame, cfg['local_cache_path'])

	logger.info('Received cached table:\n{table}\n'.format(
		table=dataFrame.apply(lambda row: HumanReadableTagCacheRow(row), axis=1).to_string()))

	dataFrame = dbConnector_v4.GetDomainMapCacheTable()
	domainCache_v4 = Cache.DomainMap.DomainMap(dataFrame, cfg['local_cache_path'])

	logger.info('Received cached table:\n{table}\n'.format(
		table=dataFrame.apply(lambda row: HumanReadableDomainCacheRow(row), axis=1).to_string()))

	dataFrame = None

	##############################
	logger.info('Initializing notification handler...')
	noteHandler = Monitor.NotifHandler.NotifHandler(dbConnector_v4, tagCache_v4, domainCache_v4, cfg['monitor_map'])

	UdpHandler.SetNoteHandler(noteHandler)

	##############################
	logger.info('Initializing UDP message handler...')
	with socketserver.ThreadingUDPServer(("127.0.0.1", cfg['mon_port']), UdpHandler.UdpHandler) as server:
		server.serve_forever()
