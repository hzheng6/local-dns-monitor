# MIT License

# Copyright (c) 2020 Haofan Zheng

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import logging

import sqlalchemy

from . import Names

class TagMap(object):
	"""
	TagMap cache. A TagMap cache should store the following:
		uid, ip_from, ip_to, tag
	"""

	def __init__(self, dataFrame, localCachePath):
		self.logger = logging.getLogger('LocalDnsMonitor.Cache.TagMap')

		self.sqlEngine = sqlalchemy.create_engine('sqlite:///{path}'.format(path=localCachePath), echo=False)

		self.tableName = 'TagMap'

		createTableQuery ='''
			CREATE TABLE "{tableName}" (
				"{uidCol}"    TEXT,
				"{ipFromCol}" INTEGER,
				"{ipToCol}"   INTEGER,
				"{tagCol}"    TEXT,
				PRIMARY KEY ("{uidCol}", "{tagCol}")
			);
		'''.format(tableName=self.tableName,
				uidCol=Names.IP2LOC_COL_UID, ipFromCol=Names.IP2LOC_COL_IP_FROM, ipToCol=Names.IP2LOC_COL_IP_TO,
				tagCol=Names.IP2LOC_COL_TAG)

		with self.sqlEngine.connect() as connection:
			connection.execute(createTableQuery)

		dataFrame.to_sql(self.tableName, con=self.sqlEngine, if_exists='append', index=False)

		self.logger.info('Local Tag Map cache initialized.')

	def IsInMap(self, addrObj, tag):
		self.logger.debug('Checking if {addr} is in the cache with tag {tagName}.'.format(addr=str(addrObj), tagName=tag))

		targetColName = 'count'
		query = '''
			SELECT count(*) AS '{targetColName}'
			FROM {tableName}
			WHERE ({ipFromCol} <= {addr}) AND ({addr} <= {ipToCol}) AND ({tagCol} = '{tagName}');
			'''.format(
			targetColName = targetColName,
			tableName = self.tableName,
			ipFromCol = Names.IP2LOC_COL_IP_FROM, ipToCol = Names.IP2LOC_COL_IP_TO, tagCol = Names.IP2LOC_COL_TAG,
			addr = int(addrObj), tagName = tag
		)

		#self.logger.debug('Executing query {queryStr}...'.format(queryStr = query))

		with self.sqlEngine.connect() as connection:
			resultPxy = connection.execute(query)

			result = resultPxy.fetchall()
			if len(result) > 0:
				return (result[0][targetColName] > 0)
			else:
				self.logger.warning('Aggregation query: {query} returned 0 row; False returned.'.format(query = query))
				return False

		self.logger.warning('Failed to create connection for query: {query}; False returned.'.format(query = query))
		return False

	def AddToMap(self, uid, ipFrom, ipTo, tag):
		self.logger.debug('Add record ({uid}, {ipFrom}, {ipTo}, {tag}) to the cache.'.format(uid=uid, ipFrom=ipFrom, ipTo=ipTo, tag=tag))

		query = '''
			INSERT INTO {tableName} ({uidCol}, {ipFromCol}, {ipToCol}, {tagCol})
			VALUES ('{uid}', {ipFrom}, {ipTo}, '{tag}');
			'''.format(
			tableName = self.tableName,
			uidCol = Names.IP2LOC_COL_UID, ipFromCol = Names.IP2LOC_COL_IP_FROM, ipToCol = Names.IP2LOC_COL_IP_TO, tagCol = Names.IP2LOC_COL_TAG,
			uid=uid, ipFrom=ipFrom, ipTo=ipTo, tag=tag
		)

		#self.logger.debug('Executing query {queryStr}...'.format(queryStr = query))

		with self.sqlEngine.connect() as connection:
			try:
				connection.execute(query)
			except sqlalchemy.exc.IntegrityError:
				self.logger.warning('UID {uid} with tag {tag} is already in the cache tag table.'.format(uid=uid, tag=tag))
			except:
				raise

			return
			#connection.execute('COMMIT;')

		self.logger.warning('Failed to create connection for query: {query}; Nothing has been added to the cache.'.format(query = query))
