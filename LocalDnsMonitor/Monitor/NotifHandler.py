# MIT License

# Copyright (c) 2020 Haofan Zheng

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import logging
import ipaddress

class NotifHandlerException(Exception):
	"""NotifHandlerException base exception"""
	pass

class UnrecognizedNoteException(NotifHandlerException):
	"""UnrecognizedNoteException exception"""
	pass

class MonitorMapParseException(NotifHandlerException):
	"""MonitorMapParseException exception"""
	pass

class MonitorMapConfigDuplicationException(NotifHandlerException):
	"""MonitorMapConfigDuplicationException exception"""
	pass

class MonitorMap(object):

	def __init__(self, mapInJson):

		self.logger = logging.getLogger("LocalDnsMonitor.Monitor.MonitorMap")

		self.map = {}
		for contryCode, v in mapInJson.items():

			if 'tags' not in v:
				raise MonitorMapParseException

			for tag in v['tags']:
				if (tag in self.map) and (self.map[tag] != contryCode):
					self.logger.error(
						'Country code {coCode} was already mapped to, but now it\'s trying to map to {coCodeNew}.'.format(
							coCode=self.map[tag] , coCodeNew=contryCode))
					raise MonitorMapConfigDuplicationException

				self.map[tag] = contryCode

		self.logger.info('Monitor Map initialized:\n\t{map}\n'.format(map=str(self.map)))

class NotifHandler(object):

	def __init__(self, dbV4, tagCacheV4, domainCacheV4, monMap):
		self.dbV4 = dbV4
		self.tagCacheV4 = tagCacheV4
		self.domainCacheV4 = domainCacheV4

		self.monMap = MonitorMap(monMap)

		self.logger = logging.getLogger("LocalDnsMonitor.Monitor.NotifHandler")

		self.logger.info("Notification handler initialized.")

	def ProcResponsibleTagNoteV4(self, addrObj, tag, domain, expCoCode):

		dbRec = None

		# 1. Check if it's in the tag cache
		if not self.tagCacheV4.IsInMap(addrObj, tag):
			# 1.0. It's not in the cache, we need to update the cache and the
			# remote database
			self.logger.debug('Ip address {ipaddr} with tag {tag} is not in the cache.'.format(ipaddr=str(addrObj), tag=tag))

			# 1.1. Get record from remote db
			dbRec = self.dbV4.GetMainRecordByAddr(addrObj)

			# 1.2. Check if the country code if what we expected
			if dbRec['country_code'] != expCoCode:
				# 1.2.0. This address is not in the country we interested in,
				# so directly return
				self.logger.warning('Ip address {ipaddr} notification received with tag {tag}, however, the expected country code "{expCoCode}" doesn\'t match the one "{dbCoCode}" in remote database.'.format(ipaddr=str(addrObj), tag=tag, expCoCode=expCoCode, dbCoCode=dbRec['country_code']))
				return

			# 1.3. First, we update the remote db
			self.logger.info('Adding uid: {uid}, addr: {addr}, tag: {tag} to remote DB...'.format(uid=dbRec['uid'], addr=str(addrObj), tag=tag))
			self.dbV4.InsertTagMapRec(dbRec['uid'], tag)

			# 1.4. Then, we update the local cache
			self.tagCacheV4.AddToMap(dbRec['uid'], dbRec['ip_from'], dbRec['ip_to'], tag)

			self.logger.debug('Ip address {ipaddr} with tag {tag} is added to the cache.'.format(ipaddr=str(addrObj), tag=tag))

		# 2. Check if it's in the domain cache
		if not self.domainCacheV4.IsInMap(addrObj, domain):
			# 2.0. It's not in the cache, we need to update the cache and the
			# remote database
			self.logger.debug('Ip address {ipaddr} with domain {domain} is not in the cache.'.format(ipaddr=str(addrObj), domain=domain))

			# 2.1. Get record from remote db
			if dbRec is None:
				dbRec = self.dbV4.GetMainRecordByAddr(addrObj)

				# 2.1.1. Check if the country code if what we expected
				if dbRec['country_code'] != expCoCode:
					self.logger.warning('Ip address {ipaddr} notification received with domain {domain}, however, the expected country code "{expCoCode}" doesn\'t match the one "{dbCoCode}" in remote database.'.format(ipaddr=str(addrObj), domain=domain, expCoCode=expCoCode, dbCoCode=dbRec['country_code']))
					return

			# 2.2. First, we update the remote db
			self.logger.info('Adding uid: {uid}, addr: {addr}, domain: {domain} to remote DB...'.format(uid=dbRec['uid'], addr=str(addrObj), domain=domain))
			self.dbV4.InsertDomainMapRec(dbRec['uid'], domain)

			# 2.3. Then, we update the local cache
			self.domainCacheV4.AddToMap(dbRec['uid'], dbRec['ip_from'], dbRec['ip_to'], domain)

			self.logger.debug('Ip address {ipaddr} with domain {domain} is added to the cache.'.format(ipaddr=str(addrObj), domain=domain))

	def Handle(self, note):
		"""
		Handle notification from DNS server

		`note` has following layout:
		{
			"type"   : "outbound",
			"q_msg"  : question.to_text(),
			"r_msg"  : response.to_text(),
			"q_name" : question_name,
			"tag"    : group['tag'],
			"match"  : matchedDomain,
			"a_list" :
			{
				"name"    : ".".join([x.decode() for x in answer.name.labels]),
				"rdType"  : dns.rdatatype.to_text(answer.rdtype),
				"rdClass" : dns.rdataclass.to_text(answer.rdclass),
				"ttl"     : answer.ttl,
				"data"    : item.to_text()
			}
		}
		"""

		if ('type' not in note) or \
			('q_name' not in note) or \
			('tag' not in note) or \
			('match' not in note) or \
			('a_list' not in note):

			errMsg = 'Received notification with unsupported format:\n\t{note}'.format(note=note)
			self.logger.warning(errMsg)
			raise UnrecognizedNoteException(errMsg)

		if (note['type'] != 'outbound') or \
			(note['tag'] not in self.monMap.map):

			self.logger.debug('Notification received doesn\'t contain data we care about:\n\t{note}'.format(note=note))
			return

		for ans in note['a_list']:

			if ('name' not in ans) or \
			('rdType' not in ans) or \
			('rdClass' not in ans) or \
			('data' not in ans):

				errMsg = 'Received notification with unsupported "a_list" format:\n\t{note}'.format(note=note)
				self.logger.warning(errMsg)
				raise UnrecognizedNoteException(errMsg)

			if (ans['rdClass'] != 'IN') or \
			(ans['rdType'] != 'A' and ans['rdType'] != 'AAAA'):

				self.logger.debug('Notification received doesn\'t contain answer we care about.'.format(note=note))

			else:

				if ans['rdType'] == 'A':
					# It's a IPv4 address

					self.ProcResponsibleTagNoteV4(ipaddress.ip_address(ans['data']), note['tag'], note['match'], self.monMap.map[note['tag']])
				else:
					# It's a IPv6 address

					self.logger.warning('Support for IPv6 addresses is not implemented yet.')

		return
