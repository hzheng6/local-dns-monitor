# MIT License

# Copyright (c) 2020 Haofan Zheng

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

class DbConnectorException(Exception):
	"""DbConnectorException base exception"""
	pass

class DbConnectionFailedException(DbConnectorException):
	pass

class InputFileMissingException(DbConnectorException):
	pass

class DbConnector(object):

	def Connect(self):
		"""
		Create and return a connection object to the database
		"""
		pass

	def GetTagMapCacheTable(self):
		"""
		GetTagMapCacheTable

		Returns:
			a dataframe used to store as cache of TagMap
		"""
		pass

	def GetDomainMapCacheTable(self):
		"""
		GetDomainMapCacheTable Returns a dataframe used to store as cache of DomainMap
		"""
		pass

	def GetMainRecordByAddr(self, addrObj):
		"""
		Get Record from main table by address.

		Args:
			addrObj: python3 ipaddress class object; please refer to the python3 API.

		Returns:
			a dict containing {"uid": ..., "ip_from": ..., "ip_to": ..., "country_code": ... }
		"""
		pass

	def GetUidByAddr(self, addrObj):
		"""
		Get UID of a given address from main table.

		Args:
			addrObj: python3 ipaddress class object; please refer to the python3 API.

		Returns:
			uid, in str
		"""
		return self.GetMainRecordByAddr(addrObj)['uid']

	def InsertTagMapRec(self, uid, tag):
		"""
		Insert a record to the TagMap table.
		"""
		pass

	def InsertDomainMapRec(self, uid, domain):
		"""
		Insert a record to the DomainMap table.
		"""
		pass
