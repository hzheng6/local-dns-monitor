# MIT License

# Copyright (c) 2020 Haofan Zheng

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import psycopg2
import pandas
import logging

from . import DbConnector
from . import Names

class Postgres(DbConnector.DbConnector):

	def __init__(self,
		addr,
		port,
		svr_ca,
		user,
		sslkey,
		sslcert,
		db,
		mainTable,
		tagTable,
		domainTable):

		self.addr = addr
		self.port = port
		self.svr_ca = svr_ca

		self.user = user
		self.sslkey = sslkey
		self.sslcert = sslcert

		self.db = db

		self.mainTable = mainTable
		self.tagTable = tagTable
		self.domainTable = domainTable

		self.logger = logging.getLogger('LocalDnsMonitor.DbConnector.Postgres')

		if (not os.path.isfile(self.svr_ca)):
			self.logger.error('Cannot find server root certificate file ({file}).'.format(file=self.svr_ca))
			raise DbConnector.InputFileMissingException

		if (not os.path.isfile(self.sslkey)):
			self.logger.error('Cannot find client SSL key file ({file}).'.format(file=self.sslkey))
			raise DbConnector.InputFileMissingException

		if (not os.path.isfile(self.sslcert)):
			self.logger.error('Cannot find client SSL certificate file ({file}).'.format(file=self.sslcert))
			raise DbConnector.InputFileMissingException

		self.logger.info('Server root certificate file: {file}.'.format(file=self.svr_ca))
		self.logger.info('Client SSL key file         : {file}.'.format(file=self.sslkey))
		self.logger.info('Client SSL certificate file : {file}.'.format(file=self.sslcert))

		connection = psycopg2.connect(
			host = self.addr,
			port = self.port,
			sslrootcert = self.svr_ca,

			user    = self.user,
			sslkey  = self.sslkey,
			sslcert = self.sslcert,

			database = self.db,

			sslmode="verify-ca")

		# if there is any issue in the connection parameters, this will raise the
		# error during initialization phase.
		connection.close()

		self.logger.info('Postgres DB connector initialized.')
		self.logger.info('Test connection to Database {username}@{addr}:{port} was successful.'.format(username=self.user, addr=self.addr, port=self.port))

	def Connect(self):
		connection = psycopg2.connect(
			host = self.addr,
			port = self.port,
			sslrootcert = self.svr_ca,

			user    = self.user,
			sslkey  = self.sslkey,
			sslcert = self.sslcert,

			database = self.db,

			sslmode="verify-ca")

		return connection

	def GetTagMapCacheTable(self):
		query = '''
			SELECT "{mainTable}"."{uid}", "{mainTable}"."{ip_from}", "{mainTable}"."{ip_to}", "{tagTable}"."{tag}"
			FROM "{mainTable}"
			INNER JOIN "{tagTable}"
			ON "{mainTable}"."{uid}" = "{tagTable}"."{uid}"
			ORDER BY "{mainTable}"."{ip_from}" ASC
			'''.format(
				mainTable = self.mainTable, tagTable = self.tagTable,
				uid = Names.IP2LOC_COL_UID, ip_from = Names.IP2LOC_COL_IP_FROM, ip_to = Names.IP2LOC_COL_IP_TO, tag = Names.IP2LOC_COL_TAG
			)

		with psycopg2.connect(
			host = self.addr,
			port = self.port,
			sslrootcert = self.svr_ca,

			user    = self.user,
			sslkey  = self.sslkey,
			sslcert = self.sslcert,

			database = self.db,

			sslmode="verify-ca") as connection:

			dataFrame = pandas.read_sql_query(query, connection)

			return dataFrame

		raise DbConnector.DbConnectionFailedException

	def GetDomainMapCacheTable(self):
		query = '''
			SELECT "{mainTable}"."{uid}", "{mainTable}"."{ip_from}", "{mainTable}"."{ip_to}", "{domainTable}"."{domain}"
			FROM "{mainTable}"
			INNER JOIN "{domainTable}"
			ON "{mainTable}"."{uid}" = "{domainTable}"."{uid}"
			ORDER BY "{mainTable}"."{ip_from}" ASC
			'''.format(
				mainTable = self.mainTable, domainTable = self.domainTable,
				uid = Names.IP2LOC_COL_UID, ip_from = Names.IP2LOC_COL_IP_FROM, ip_to = Names.IP2LOC_COL_IP_TO, domain = Names.IP2LOC_COL_DOMAIN
			)

		with psycopg2.connect(
			host = self.addr,
			port = self.port,
			sslrootcert = self.svr_ca,

			user    = self.user,
			sslkey  = self.sslkey,
			sslcert = self.sslcert,

			database = self.db,

			sslmode='verify-ca') as connection:

			dataFrame = pandas.read_sql_query(query, connection)

			return dataFrame

		raise DbConnector.DbConnectionFailedException

	def GetMainRecordByAddr(self, addrObj):
		query = '''
			SELECT "{uidCol}", "{ipFromCol}", "{ipToCol}", "{countryCodeCol}"
			FROM "{mainTable}"
			WHERE ("{ipFromCol}" <= {addr}) AND ({addr} <= "{ipToCol}");
			'''.format(
				mainTable = self.mainTable,
				uidCol = Names.IP2LOC_COL_UID, ipFromCol = Names.IP2LOC_COL_IP_FROM, ipToCol = Names.IP2LOC_COL_IP_TO, countryCodeCol = Names.IP2LOC_COL_C_CODE,
				addr = int(addrObj)
			)

		with psycopg2.connect(
			host = self.addr,
			port = self.port,
			sslrootcert = self.svr_ca,

			user    = self.user,
			sslkey  = self.sslkey,
			sslcert = self.sslcert,

			database = self.db,

			sslmode='verify-ca') as connection:

			with connection.cursor() as cursor:
				cursor.execute(query)
				rawRes = cursor.fetchone()

				return {
					'uid'          : rawRes[0],
					'ip_from'      : rawRes[1],
					'ip_to'        : rawRes[2],
					'country_code' : rawRes[3]
				}

			raise DbConnector.DbConnectionFailedException

		raise DbConnector.DbConnectionFailedException

	def InsertTagMapRec(self, uid, tag):
		query = '''
			INSERT INTO "{tagTable}" ("{uidCol}", "{tagCol}")
			VALUES ('{uid}', '{tag}');
		'''.format(
			tagTable = self.tagTable,
			uidCol = Names.IP2LOC_COL_UID, tagCol = Names.IP2LOC_COL_TAG,
			uid = uid, tag = tag
		)

		with psycopg2.connect(
			host = self.addr,
			port = self.port,
			sslrootcert = self.svr_ca,

			user    = self.user,
			sslkey  = self.sslkey,
			sslcert = self.sslcert,

			database = self.db,

			sslmode='verify-ca') as connection:

			with connection.cursor() as cursor:
				try:
					cursor.execute(query)
				except psycopg2.errors.UniqueViolation:
					connection.rollback()
					self.logger.warning('UID {uid} with tag {tag} is already in the remote database domain table.'.format(uid=uid, tag=tag))
				except:
					connection.rollback()
					raise
				else:
					connection.commit()

				return

			raise DbConnector.DbConnectionFailedException

		raise DbConnector.DbConnectionFailedException

	def InsertDomainMapRec(self, uid, domain):
		query = '''
			INSERT INTO "{domainTable}" ("{uidCol}", "{domainCol}")
			VALUES ('{uid}', '{domain}');
		'''.format(
			domainTable = self.domainTable,
			uidCol = Names.IP2LOC_COL_UID, domainCol = Names.IP2LOC_COL_DOMAIN,
			uid = uid, domain = domain
		)

		with psycopg2.connect(
			host = self.addr,
			port = self.port,
			sslrootcert = self.svr_ca,

			user    = self.user,
			sslkey  = self.sslkey,
			sslcert = self.sslcert,

			database = self.db,

			sslmode='verify-ca') as connection:

			with connection.cursor() as cursor:
				try:
					cursor.execute(query)
				except psycopg2.errors.UniqueViolation:
					connection.rollback()
					self.logger.warning('UID {uid} with domain {domain} is already in the remote database domain table.'.format(uid=uid, domain=domain))
				except:
					connection.rollback()
					raise
				else:
					connection.commit()

				return

			raise DbConnector.DbConnectionFailedException

		raise DbConnector.DbConnectionFailedException
