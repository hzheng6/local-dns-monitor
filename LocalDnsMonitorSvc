#!/bin/sh

### BEGIN INIT INFO
# Provides:          LocalDnsMonitor
# Required-Start:    $local_fs $network $named $time $syslog
# Required-Stop:     $local_fs $network $named $time $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Description:       LocalDnsMonitor Service
### END INIT INFO

# Configurable parameters
DAEMON_NAME=LocalDnsMonitor
SCRIPT=/usr/bin/python3
SCRIPT_OPTIONS="/path/to/main.py -c <Config> -l <Log>"


set -e


PIDFILE=/run/${DAEMON_NAME}.pid

. /lib/lsb/init-functions

is_service_running()
{
	if [ -f "${PIDFILE}" ]; then
		if ps -p $(cat "${PIDFILE}") > /dev/null 2>&1
		then
			return 0
		fi

		rm -f "${PIDFILE}"
	fi

	return 1
}

start() {

	if is_service_running ; then
		echo "${DAEMON_NAME} already running" >&2
		log_action_msg "${DAEMON_NAME} already running" || true
		return 1
	fi

	echo "Starting ${DAEMON_NAME} daemon..." >&2
	log_daemon_msg "Starting ${DAEMON_NAME} daemon..." "${DAEMON_NAME}" || true
	if start-stop-daemon --start --quiet --oknodo --background --pidfile "${PIDFILE}" --make-pidfile --exec "${SCRIPT}" -- ${SCRIPT_OPTIONS}; then
		echo "${DAEMON_NAME} started" >&2
		log_end_msg 0 || true
	else
		echo "${DAEMON_NAME} failed to start" >&2
		log_end_msg 1 || true
	fi
}

stop() {
	if ! is_service_running ; then
		echo "${DAEMON_NAME} not running" >&2
		log_action_msg "${DAEMON_NAME} not running" || true
		return 1
	fi

	echo "Stopping ${DAEMON_NAME} daemon..." >&2
	log_daemon_msg "Stopping ${DAEMON_NAME} daemon..." "${DAEMON_NAME}" || true
	if start-stop-daemon --stop --quiet --oknodo --retry 10 --pidfile "${PIDFILE}" --remove-pidfile ; then
		echo "${DAEMON_NAME} stopped" >&2
		log_end_msg 0 || true
	else
		echo "${DAEMON_NAME} failed to stop" >&2
		log_end_msg 1 || true
	fi

}

restart() {
	if is_service_running ; then
		if ! stop ; then
			echo "${DAEMON_NAME} failed to restart" >&2
			return 1
		fi
	fi

	start
}

case "$1" in
	start)
		start
		;;
	stop)
		stop
		;;
	restart)
		restart
		;;
	*)
		echo "Usage: $0 {start|stop|restart}"
esac
