#! /usr/bin/python3

# MIT License

# Copyright (c) 2020 Haofan Zheng

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import sys
import logging
import argparse

import LocalDnsMonitor.main


def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('-c', '--config',     required=True,  type=str,  help="Path to config file.")
	parser.add_argument('-l', '--log',        required=False, type=str,  help="Path to log file.")
	parser.add_argument('-d', '--debug',      required=False,            help="Enable logging debug message.",  default=False, action='store_true')
	args = parser.parse_args()

	if args.log is None:
		logging.basicConfig(format='[%(asctime)s][%(name)s](%(levelname)s) %(message)s',
							level='DEBUG' if args.debug else 'INFO',
							stream=sys.stdout)
	else:
		logging.basicConfig(format='[%(asctime)s][%(name)s](%(levelname)s) %(message)s',
							level='DEBUG' if args.debug else 'INFO',
							filename=args.log)

	LocalDnsMonitor.main.main(args.config)

if __name__ == "__main__":
	main()