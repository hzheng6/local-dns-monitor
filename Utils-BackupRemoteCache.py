#! /usr/bin/python3

# MIT License

# Copyright (c) 2020 Haofan Zheng

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import sys
import csv
import logging
import argparse

import pandas

import LocalDnsMonitor.Config
import LocalDnsMonitor.DbConnector

def BackupOne(dataFrame, filePath, logger):

	if os.path.isfile(filePath):
		# We have existing backup to merge


		logger.info('Merging with existing backup at {filePath}...'.format(filePath=filePath))
		exDataFrame = pandas.read_csv(filePath)

		dataFrame = pandas.concat([dataFrame, exDataFrame]).drop_duplicates().reset_index(drop=True)

	dataFrame.to_csv(filePath, header=True, index=False, quoting=csv.QUOTE_NONNUMERIC)

def Backup(configPath):

	logger = logging.getLogger('BackupRemoteCache.Backup')

	cfg = LocalDnsMonitor.Config.ReadConfig(configPath)

	##############################
	logger.info('Initializing remote database connector...')
	dbConnector_v4 = None
	if cfg['DB']['db_type'] == 'postgres':
		dbConnector_v4 = LocalDnsMonitor.DbConnector.Postgres.Postgres(
			addr   = cfg['DB']['addr'],
			port   = cfg['DB']['port'],
			svr_ca = cfg['DB']['svr_ca'],

			user    = cfg['DB']['user'],
			sslkey  = cfg['DB']['sslkey'],
			sslcert = cfg['DB']['sslcert'],

			db          = cfg['DB']['db'],
			mainTable   = cfg['DB']['main_table'],
			tagTable    = cfg['DB']['tagmap_table'],
			domainTable = cfg['DB']['domain_table']
		)
	else:
		logger.error('DbConnector type {type} is not supported in current implementation.'.format(type = cfg['DB']['db_type']))
		exit(-1)

	dataFrame = dbConnector_v4.GetTagMapCacheTable()

	logger.info('Backing up tag map cache...')
	BackupOne(dataFrame, 'TagMapCache.bak.csv', logger)

	dataFrame = dbConnector_v4.GetDomainMapCacheTable()

	logger.info('Backing up domain map cache...')
	BackupOne(dataFrame, 'DomainMapCache.bak.csv', logger)


def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('-c', '--config',     required=True,  type=str, help="Path to config file.")
	parser.add_argument('-l', '--log',        required=False, type=str, help="Path to log file.")
	# parser.add_argument('-o', '--output',     required=True,  type=str, help="Path to output file.")
	args = parser.parse_args()

	if args.log is None:
		logging.basicConfig(format='[%(asctime)s][%(name)s](%(levelname)s) %(message)s',
							level='DEBUG',
							stream=sys.stdout)
	else:
		logging.basicConfig(format='[%(asctime)s][%(name)s](%(levelname)s) %(message)s',
							level='DEBUG',
							filename=args.log)

	Backup(args.config)

if __name__ == "__main__":
	main()
